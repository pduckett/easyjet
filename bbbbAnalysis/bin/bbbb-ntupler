#!/bin/env python

#
# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#

#
# Extends easyjet-ntupler with dihiggs-specific algorithms
#

import sys
import time
import pathlib

from EasyjetHub.hub import (
    AnalysisArgumentParser,     # lives in ArgumentParser
    analysis_configuration,     # lives in flags
    run_job,                    # lives in run
    default_sequence_cfg,       # lives in main_sequence_config
)
from EasyjetHub.output.ttree.minituple_config import minituple_cfg
from EasyjetHub.output.h5.h5_config import get_h5_cfg
from EasyjetHub.output.xaod import get_xaod_cfg
from EasyjetHub.algs.event_counter_config import event_counter_cfg

from bbbbAnalysis.utils.option_validation import validate_hh4b_analysis_prerequisites
from bbbbAnalysis.config.dihiggs import dihiggs_cfg
from bbbbAnalysis.config.boosted import boosted_branches
from bbbbAnalysis.config.resolved import resolved_branches
from EasyjetHub.algs.orthogonality.orth_config import orth_branches
from EasyjetHub.algs.orthogonality.orth_config import orth_cfg

def hh4b_parser():
    # Get and customise the parser
    parser = AnalysisArgumentParser()
    parser.add_analysis_arg(
        "--output-hists",
        metavar="HISTS",
        type=pathlib.Path,
        default=False,
        help="save histograms"
    )
    return parser


def hh4b_job_cfg():
    parser = hh4b_parser()

    # Fill the configuration flags from the
    # parsed arguments.
    # Returned flags are locked
    flags, args = analysis_configuration(parser)

    validate_hh4b_analysis_prerequisites(flags)

    # Get a standard ComponentAccumulator with the following infrastructure:
    # - basic services for event loop, messaging etc
    # - apply preselection on triggers and data quality
    # - build event info with calibrations etc
    seqname = "HH4bSeq"
    cfg = default_sequence_cfg(flags, seqname)

    cfg.merge(
        dihiggs_cfg(flags),
        seqname,
    )

    # Optionally run orthogonality check (only runs if you set do_orth_check to true in your RunConfig)
    extra_orth_branches = []
    if(flags.Analysis.orthogonality.do_orth_check):
        cfg.merge(
            orth_cfg(
                flags,
                smalljetkey=flags.Analysis.container_names.output.reco4PFlowJet,
                photonkey=flags.Analysis.container_names.output.photons,
            ),
            seqname
        )
        
        extra_orth_branches = orth_branches(flags)

    # Determine the list of output branches
    # For more sophistication this could be done using
    # custom BranchManagers
    bbbb_branches = []
    for tree_flags in flags.Analysis.ttree_output:
        if flags.Analysis.do_boosted_dihiggs:
            bbbb_branches += boosted_branches(flags)
        if flags.Analysis.do_resolved_dihiggs:
            bbbb_branches += resolved_branches(flags)
        if(flags.Analysis.orthogonality.do_orth_check):
            bbbb_branches += extra_orth_branches

    # Explicitly call the minituple config because we want to provide
    # additional branches, the list of which is generated dynamically
    # rather than statically in the yaml
    if flags.Analysis.out_file:
        for tree_flags in flags.Analysis.ttree_output:
            cfg.merge(
                minituple_cfg(
                    flags,tree_flags,
                    flags.Analysis.out_file,
                    extra_output_branches = bbbb_branches
                ),
                seqname,
            )

    if flags.Analysis.h5_output:
        cfg.merge(
            get_h5_cfg(flags),
            seqname,
        )

    if flags.Output.AODFileName:
        cfg.merge(
            get_xaod_cfg(flags, seqname),
            seqname
        )

    # Record the number of events for logging and tests
    cfg.merge(event_counter_cfg("n_events"), seqname)

    return cfg, flags, args


def main():
    # record the total run time
    starttime = time.process_time()

    cfg, flags, args = hh4b_job_cfg()

    if args.dry_run:
        return_code = 1
    else:
        return_code = run_job(flags, args, cfg)

    # this is for unit tests: if jobs run too long something is wrong
    if args.timeout:
        duration = time.process_time() - starttime
        if duration > args.timeout:
            raise RuntimeError(
                f"runtime ({duration:.1f}s) exceed timeout ({args.timeout:.0f}s)"
            )
    return return_code.isSuccess()


# Execute the main function if this file was executed as a script
if __name__ == "__main__":
    is_successful = main()
    sys.exit(0 if is_successful else 1)
