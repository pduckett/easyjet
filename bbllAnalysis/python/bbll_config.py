from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
import AthenaCommon.SystemOfUnits as Units

from EasyjetHub.output.ttree.selected_objects import (
    get_selected_objects_branches_variables,
)


def bbll_cfg(flags, smalljetkey, muonkey, electronkey,
             float_variables=[], int_variables=[], float_NW_variables=[]):

    cfg = ComponentAccumulator()

    MuonWPLabel = f'{flags.Analysis.Muon.ID}_{flags.Analysis.Muon.Iso}'
    cfg.addEventAlgo(
        CompFactory.Easyjet.MuonSelectorAlg(
            "MuonSelectorAlg",
            containerInKey=MuonWPLabel + muonkey,
            containerOutKey="bbllAnalysisMuons_%SYS%",
            muon_WP=MuonWPLabel,
            isMC=flags.Input.isMC,
            checkOR=flags.Analysis.do_overlap_removal,
            minPt=9 * Units.GeV,
        )
    )

    ElectronWPLabel = f'{flags.Analysis.Electron.ID}_{flags.Analysis.Electron.Iso}'
    cfg.addEventAlgo(
        CompFactory.Easyjet.ElectronSelectorAlg(
            "ElectronSelectorAlg",
            containerInKey=ElectronWPLabel + electronkey,
            containerOutKey="bbllAnalysisElectrons_%SYS%",
            checkOR=flags.Analysis.do_overlap_removal,
            ele_WP=ElectronWPLabel,
            isMC=flags.Input.isMC,
            minPt=9 * Units.GeV,
        )
    )

    cfg.addEventAlgo(
        CompFactory.Easyjet.JetSelectorAlg(
            "SmallJetSelectorAlg",
            containerInKey=smalljetkey,
            containerOutKey="bbllAnalysisJets_%SYS%",
            bTagWPDecorName="",
            minPt=20 * Units.GeV,
            minimumAmount=2,  # -1 means ignores this
            checkOR=flags.Analysis.do_overlap_removal,
        )
    )

    from EasyjetHub.algs.postprocessing.trigger_matching import TriggerMatchingToolCfg

    # Selection
    trigger_branches = [
        f"trigPassed_{c.replace('-', '_').replace('.', 'p')}"
        for c in flags.Analysis.TriggerChains
    ]

    # Selection
    cfg.addEventAlgo(
        CompFactory.HHBBLL.HHbbllSelectorAlg(
            "HHbbllSelectorAlg",
            jets="bbllAnalysisJets_%SYS%",
            muons="bbllAnalysisMuons_%SYS%",
            electrons="bbllAnalysisElectrons_%SYS%",
            met="AnalysisMET_%SYS%",
            bTagWPDecorName="ftag_select_" + flags.Analysis.small_R_jet.btag_wp,
            eventDecisionOutputDecoration="bbll_pass_sr_%SYS%",
            cutList=flags.Analysis.CutList,
            saveCutFlow=flags.Analysis.save_bbll_cutflow,
            isMC=flags.Input.isMC,
            triggerLists=trigger_branches,
            trigMatchingTool=cfg.popToolsAndMerge(TriggerMatchingToolCfg(flags)),
            bypass=(flags.Analysis.bypass if hasattr(flags.Analysis, 'bypass')
                    else False),
        )
    )

    # MMC decoration
    if flags.Analysis.do_mmc:
        cfg.addEventAlgo(
            CompFactory.HHBBLL.MMCDecoratorAlg(
                "MMCDecoratorAlg",
                jets="bbllAnalysisJets_%SYS%",
                muons="bbllAnalysisMuons_%SYS%",
                electrons="bbllAnalysisElectrons_%SYS%",
                met="AnalysisMET_%SYS%",
            )
        )

    # NeutrinoWeighting
    if flags.Analysis.NeutrinoWeighting.doNW:
        NeutrinoWeightingTool_1 = CompFactory.HHBBLL.NeutrinoWeightingTool(
            "NeutrinoWeightingTool_1",
            resolution_settings=flags.Analysis.NeutrinoWeighting.resolution_settings,
            resolution_number=flags.Analysis.NeutrinoWeighting.resolution_number)
        NeutrinoWeightingTool_2 = CompFactory.HHBBLL.NeutrinoWeightingTool(
            "NeutrinoWeightingTool_2",
            resolution_settings=flags.Analysis.NeutrinoWeighting.resolution_settings,
            resolution_number=flags.Analysis.NeutrinoWeighting.resolution_number)
        cfg.addEventAlgo(
            CompFactory.HHBBLL.NeutrinoWeightingAlg(
                "NeutrinoWeightingAlg",
                jets="bbllAnalysisJets_%SYS%",
                muons="bbllAnalysisMuons_%SYS%",
                electrons="bbllAnalysisElectrons_%SYS%",
                met="AnalysisMET_%SYS%",
                bTagWPDecorName="ftag_select_" + flags.Analysis.small_R_jet.btag_wp,
                floatNWVariables=float_NW_variables,
                NW_cutList=flags.Analysis.NeutrinoWeighting.cutList,
                NeutrinoWeightingTools=[
                    NeutrinoWeightingTool_1,
                    NeutrinoWeightingTool_2],
            )
        )

    # calculate final bbll vars
    cfg.addEventAlgo(
        CompFactory.HHBBLL.BaselineVarsbbllAlg(
            "FinalVarsbbllAlg",
            isMC=flags.Input.isMC,
            jets="bbllAnalysisJets_%SYS%",
            muons="bbllAnalysisMuons_%SYS%",
            electrons="bbllAnalysisElectrons_%SYS%",
            muonWP=MuonWPLabel,
            eleWP=ElectronWPLabel,
            met="AnalysisMET_%SYS%",
            bTagWPDecorName="ftag_select_" + flags.Analysis.small_R_jet.btag_wp,
            floatVariableList=float_variables,
            intVariableList=int_variables
        )
    )

    return cfg


def get_BaselineVarsbbllAlg_variables(flags):
    float_variable_names = []
    int_variable_names = []

    for object in ["ee", "mumu", "emu", "bb"]:
        for var in ["m", "pT", "dR", "Eta", "Phi"]:
            float_variable_names.append(f"{var}{object}")

    float_variable_names += ["mll", "pTll", "met_x", "met_y", "mbbll", "dRll",
                             "mbbllmet", "MET_sig", "mT_Lepton1_Met", "mT_Lepton2_Met",
                             "mT_L_min", "dRbl_min", "HT2", "HT2r", "mT2_bb", "mbl"]

    int_variable_names += ["nJets", "nBJets", "nElectrons", "nMuons", "nCentralJets"]

    return float_variable_names, int_variable_names


def get_BaselineVarsbbllAlg_highlevelvariables(flags):
    high_level_float_variables = []
    high_level_int_variables = []

    return high_level_float_variables, high_level_int_variables


def bbll_branches(flags):
    branches = []

    # this will be all the variables that are calculated by the
    # BaselineVarsbbllAlg algorithm
    all_baseline_variable_names = []
    float_variable_names = []
    int_variable_names = []

    # these are the variables that will always be stored by easyjet specific to HHbbtt
    # further below there are more high level variables which can be
    # stored using the flag
    # flags.Analysis.store_high_level_variables
    baseline_float_variables, baseline_int_variables \
        = get_BaselineVarsbbllAlg_variables(flags)
    float_variable_names += baseline_float_variables
    int_variable_names += baseline_int_variables

    if flags.Analysis.do_mmc:
        # do not append mmc variables to float_variable_names
        # or int_variable_names as they are stored by the
        # mmc algortithm not BaselineVarsbbllAlg
        for var in ["status", "pt", "eta", "phi", "m"]:
            all_baseline_variable_names.append(f"mmc_{var}")

    float_NW_variable_names = []
    if flags.Analysis.NeutrinoWeighting.doNW:
        # do not append TopReco variables to float_variable_names
        # or int_variable_names as they are stored by the
        # TopReco algortithm not BaselineVarsbbllAlg
        all_baseline_variable_names.append("NW_solutions")
        for var in ["neutrinoweight", "top_pt", "top_eta", "top_phi",
                    "top_e", "tbar_pt", "tbar_eta", "tbar_phi", "tbar_e", "ttbar_pt",
                    "ttbar_eta", "ttbar_phi", "ttbar_e", "nu_pt", "nu_eta", "nu_phi",
                    "nu_e", "nubar_pt", "nubar_eta", "nubar_phi", "nubar_e"]:
            float_NW_variable_names.append(f"NW_{var}")
    if flags.Analysis.store_high_level_variables:
        high_level_float_variables, high_level_int_variables \
            = get_BaselineVarsbbllAlg_highlevelvariables(flags)
        float_variable_names += high_level_float_variables
        int_variable_names += high_level_int_variables

    all_baseline_variable_names += [
        *float_variable_names,
        *int_variable_names,
        *float_NW_variable_names]

    for tree_flags in flags.Analysis.ttree_output:
        for var in all_baseline_variable_names:
            branches += [f"EventInfo.{var}_%SYS% -> bbll_{var}"
                         + flags.Analysis.systematics_suffix_separator + "%SYS%"]

    # These are the variables always saved with the objects selected by the analysis
    # This is tunable with the flags amount and variables
    # in the object configs.
    object_level_branches, object_level_float_variables, object_level_int_variables \
        = get_selected_objects_branches_variables(flags, "bbll")
    float_variable_names += object_level_float_variables
    int_variable_names += object_level_int_variables

    branches += object_level_branches

    branches += ["EventInfo.bbll_pass_sr_%SYS% -> bbll_pass_SR"
                 + flags.Analysis.systematics_suffix_separator + "%SYS%"]

    if (flags.Analysis.save_bbll_cutflow):
        cutList = flags.Analysis.CutList + flags.Analysis.Categories
        for cut in cutList:
            branches += [f"EventInfo.{cut}_%SYS% -> bbll_{cut}"
                         + flags.Analysis.systematics_suffix_separator + "%SYS%"]

    # trigger variables do not need to be added to variable_names
    # as it is written out in HHbbllSelectorAlg
    for cat in ["SLT", "DLT", "ASLT1_em", "ASLT1_me", "ASLT2"]:
        branches += \
            [f"EventInfo.pass_trigger_{cat}_%SYS% -> bbll_pass_trigger_{cat}"
             + flags.Analysis.systematics_suffix_separator + "%SYS%"]

    return branches, float_variable_names, int_variable_names, float_NW_variable_names
